var express = require('express'),
    fs = require("fs"),
    crypto = require("crypto"),
    multiparty = require('multiparty'),
    GoogleURL = require('./libs/google');
var app = express();
var googleUrl = new GoogleURL( { key: 'AIzaSyAZcgPPqFrODow5f1PGeNOvWFBFPFzYwHI' });

app.use(express.static('public'));

app.get("/", function(req,res) {
    res.sendFile('app.html',{ root: __dirname+"/public/views" });
});
app.get("/uploads/:filename", function(req,res) {
    res.sendFile(req.params.filename,{ root: __dirname+"/public/uploads" });
});




app.post('/', function(req, res) {
    
    var form = new multiparty.Form();
    var uploadFile = {uploadPath: '', type: '', size: 0};
    var maxSize = 2 * 1024 * 1024;
    var errors = [];
    var loadedFileName = '';
    var originalFileName = '';
    
    form.on('error', function(err) {
        if(fs.existsSync(uploadFile.path)) {
            fs.unlinkSync(uploadFile.path);
            console.log('error');
        }
    });

    form.on('close', function() {
        if(errors.length == 0) {
            googleUrl.shorten( 'http://localhost:3000/uploads/'+loadedFileName, function (err, url) {
                res.send({status: 'success', filename: originalFileName, url: url});
            });
        }
        else {
            if(fs.existsSync(uploadFile.path)) {
                fs.unlinkSync(uploadFile.path);
            }
            res.send({status: 'bad', errors: errors});
        }
    });
    
    form.on('part', function(part) {
        uploadFile.size = part.byteCount;
        uploadFile.type = part.headers['content-type'];
        originalFileName = part.filename;
        loadedFileName = makehash(crypto, part.filename)+'.'+part.filename.split('.').pop();
        uploadFile.path = './public/uploads/' + loadedFileName;
        
        if(uploadFile.size > maxSize) {
            errors.push('File size is ' + uploadFile.size + '. Limit is' + (maxSize / 1024 / 1024) + 'MB.');
        }
        
        if(errors.length == 0) {
            var out = fs.createWriteStream(uploadFile.path);
            part.pipe(out);
        }
        else {
            part.resume();
        }
    });
    form.parse(req);
});
app.listen(3000);


function makehash(crypto, filename) {
    var current_date = (new Date()).valueOf().toString();
    var random = Math.random().toString();
    var str =  crypto.createHash('md5').update(filename + current_date + random).digest('hex');
    return str.substr(0, str.length - 20);
}
